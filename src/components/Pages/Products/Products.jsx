import React from "react";
import { useDispatch, useSelector } from "react-redux";

import "./index.css";

export default function Products() {
  const selector = useSelector((state) => state);
  const dispatch = useDispatch();

  const products = selector.products;
  console.log(products);
  const cartProducts = selector.cartProducts;

  const addToCart = (id) => {
    let isInCart = false;
    cartProducts.forEach((el) => {
      if (id === el.id) isInCart = true;
    });
    if (!isInCart) {
      dispatch({ type: "ADD_TO_CART", payload: products.find((product) => id === product.id) });
    }
    console.log(cartProducts);
    alert("Product add to Cart");
  };
  return (
    <div className="productsList">
      {products.map((item) => (
        <div className="product" key={item.id}>
          <img src={item.image} alt="product-image" />
          <h3>{item.name}</h3>
          <span>Color: {item.color}</span>

          <div className="priceBlock">
            <span className="priceProduct">Price: {item.price}$</span>
            <button onClick={() => addToCart(item.id)}>BUY</button>
          </div>
        </div>
      ))}
    </div>
  );
}
