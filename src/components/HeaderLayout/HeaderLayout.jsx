import React from "react";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import "./index.css";

export default function HeaderLayout() {
  const selector = useSelector((state) => state);
  const cartProducts = selector.cartProducts;

  return (
    <header id="header">
      <NavLink to="/products">
        <img className="siteLogo" src="https://img.icons8.com/bubbles/344/mac-os.png" alt="Products" />
      </NavLink>

      <form className="search">
        <input type="text" placeholder="Search" />
        <button>Accept</button>
      </form>

      <div className="cartLink">
        <img src="https://img.icons8.com/bubbles/344/shopping-cart.png" alt="Cart" />
        <NavLink className="cart-link" to="/cart">
          Cart
        </NavLink>
        <span>{cartProducts.length}</span>
      </div>
    </header>
  );
}
