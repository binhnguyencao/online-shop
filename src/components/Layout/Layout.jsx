import React from "react";
import HeaderLayout from "../HeaderLayout/HeaderLayout";
import { Outlet } from "react-router-dom";

export default function Layout() {
  return (
    <div>
      <>
        <HeaderLayout />
        <Outlet />
      </>
    </div>
  );
}
