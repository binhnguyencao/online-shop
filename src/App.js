import { BrowserRouter, Routes, Route } from "react-router-dom";
// import HomePage from "./components/Pages/Home/HomePage";
import Cart from "./components/Pages/Cart/Cart";
import Products from "./components/Pages/Products/Products";
import Layout from "./components/Layout/Layout";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route path="/products" element={<Products />}></Route>
            <Route path="/cart" element={<Cart />}></Route>
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
